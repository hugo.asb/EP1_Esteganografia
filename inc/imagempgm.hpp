#ifndef IMAGEMPGM_H
#define IMAGEMPGM_H

#include "imagem.hpp"

class ImagemPgm : public Imagem 
{
	private:
		int posicao_inicial;

	public:
		ImagemPgm();
		~ImagemPgm();
		ImagemPgm(string caminho_imagem);

		void setPosicaoInicial(int posicao_inicial);
		int getPosicaoInicial();

		void PegaInicio();		//Pega a posicao onde a mensagem comeca
		void DecifraMensagem();
		//Virtual
		void VerificaImagem();
		void TamanhoImagem();
};

#endif