#ifndef IMAGEM_H
#define IMAGEM_H

#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>

using namespace std;

class Imagem 
{
	private:
		string string_arquivo;
		string caminho_imagem;	//Endereço de onde a imagem se encontra em disco
		string numero;		//Numero magico (P5, P6)
		int largura;
		int altura;
		int numero_caracteres;
		int tamanho_imagem;
		int tamanho_cabecalho;
		int cores;

	public:
		Imagem();
		virtual ~Imagem();

		//Metodos acessores - Atributos
		void setStringArquivo(string string_arquivo);
		string getStringArquivo();

		void setCaminhoImagem(string caminho_imagem);
		string getCaminhoImagem();

		void setNumero(string numero);
		string getNumero();

		void setLargura(int largura);
		int getLargura();

		void setAltura(int altura);
		int getAltura();

		void setNumeroCaracteres(int numero_caracteres);
		int getNumeroCaracteres();

		void setTamanhoImagem(int tamanho_imagem);
		int getTamanhoImagem();

		void setTamanhoCabecalho(int tamanho_cabecalho);
		int getTamanhoCabecalho();

		void setCores(int cores);
		int getCores();

		//Outros Metodos
		void ManipulaArquivo();		//Abre e fecha o arquivo
		void DadosCabecalho();  //Extrai dados do cabeçalho como, numero magico, altura, largura, etc
		virtual void VerificaImagem() = 0;	//Verifica se o numero magico corresponde com o tipo da imagem
		virtual void TamanhoImagem() = 0;	
};

#endif