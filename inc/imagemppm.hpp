#ifndef IMAGEMPPM_H
#define IMAGEMPPM_H

#include "imagem.hpp"

class ImagemPpm : public Imagem 
{

	public:
		ImagemPpm();
		~ImagemPpm();
		ImagemPpm(string caminho_imagem);

		void Filtros();
		//Virtual
		void VerificaImagem();
		void TamanhoImagem();

};

#endif