#include "imagemppm.hpp"

ImagemPpm::ImagemPpm() 
{
	setStringArquivo("");
	setCaminhoImagem("");	
	setNumero("");		
	setLargura(0);
	setAltura(0);
	setNumeroCaracteres(0);
	setCores(0);
	setTamanhoImagem(0);
	setTamanhoCabecalho(0);
}

ImagemPpm::~ImagemPpm()
{

}

ImagemPpm::ImagemPpm(string caminho_imagem)
{
	setStringArquivo("");
	setCaminhoImagem(caminho_imagem);	
	setNumero("");		
	setLargura(0);
	setAltura(0);
	setNumeroCaracteres(0);
	setCores(0);
	setTamanhoImagem(0);
	setTamanhoCabecalho(0);
}

void ImagemPpm::VerificaImagem()
{
	if(getNumero() != "P6")
	{
		cout << "Essa imagem nao e do tipo .ppm!" << endl << endl;
		exit(0);
	}
}

void ImagemPpm::TamanhoImagem()
{
	int tamanho = 3 * getLargura() * getAltura();
	setTamanhoImagem(tamanho);
	//cout << tamanho << endl;
}

void ImagemPpm::Filtros()
{
	int filtro_escolhido;
	size_t dimensao_caminho = getCaminhoImagem().size();
	string nova_imagem; 
	int j;
	int zera_pixel = 0;	// Define se o pixel sera zerado ou nao

	cout << "Entre com 1 para utilizar o filtro vermelho." << endl;
	cout << "Entre com 2 para utilizar o filtro verde." << endl;
	cout << "Entre com 3 para utilizar o filtro azul." << endl;
	cout << "Tipo de filtro: ";
	cin >> filtro_escolhido;

	if(filtro_escolhido == 1)	//Vermelho
	{
		for(size_t i = 0; i < dimensao_caminho - 4; i++)	//Nome da nova imagem
		{
			nova_imagem += getCaminhoImagem()[i];
		}
		nova_imagem += "R.ppm";

		ofstream vermelho;
		vermelho.open(nova_imagem.c_str());
		string string_arquivo = getStringArquivo();

		for(j = 0; j < getTamanhoCabecalho(); j++)		//Pega cabecalho
		{
			vermelho << string_arquivo[j];
		}
			for(j = j; j <= getNumeroCaracteres(); j++)		//Modifica imagem
			{
			zera_pixel++;

				if(zera_pixel == 1)
				{
					vermelho << string_arquivo[j];
				}

				else if(zera_pixel == 2)
				{
					vermelho << 0;
				}

				else 
				{
					vermelho << 0;
					zera_pixel = 0;
				}
			}

		vermelho.close();
		cout << "Nova imagem foi salva no mesmo diretorio da imagem original" << endl;	
	}

//Verde
if(filtro_escolhido == 2)	
	{
		for(size_t i = 0; i < dimensao_caminho - 4; i++)	//Nome da nova imagem
		{
			nova_imagem += getCaminhoImagem()[i];
		}
		nova_imagem += "G.ppm";

		ofstream verde;
		verde.open(nova_imagem.c_str());
		string string_arquivo = getStringArquivo();

		for(j = 0; j < getTamanhoCabecalho(); j++)		//Pega cabecalho
		{
			verde << string_arquivo[j];
		}
			for(j = j; j <= getNumeroCaracteres(); j++)		//Modifica imagem
			{
			zera_pixel++;

				if(zera_pixel == 1)
				{
					verde << 0;
				}

				else if(zera_pixel == 2)
				{
					verde << string_arquivo[j];
				}

				else 
				{
					verde << 0;
					zera_pixel = 0;
				}
			}

		verde.close();
		cout << "Nova imagem foi salva no mesmo diretorio da imagem original" << endl;	
	}

//Azul
if(filtro_escolhido == 3)	
	{
		for(size_t i = 0; i < dimensao_caminho - 4; i++)	//Nome da nova imagem
		{
			nova_imagem += getCaminhoImagem()[i];
		}
		nova_imagem += "G.ppm";

		ofstream azul;
		azul.open(nova_imagem.c_str());
		string string_arquivo = getStringArquivo();

		for(j = 0; j < getTamanhoCabecalho(); j++)		//Pega cabecalho
		{
			azul << string_arquivo[j];
		}
			for(j = j; j <= getNumeroCaracteres(); j++)		//Modifica imagem
			{
			zera_pixel++;

				if(zera_pixel == 1)
				{
					azul << 0;
				}

				else if(zera_pixel == 2)
				{
					azul << 0;
				}

				else 
				{
					azul << string_arquivo[j];
					zera_pixel = 0;
				}
			}

		azul.close();
		cout << "Nova imagem foi salva no mesmo diretorio da imagem original" << endl;	
	}
}
