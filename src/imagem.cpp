#include "imagem.hpp"


Imagem::Imagem()
{
	 string_arquivo = "";
	 caminho_imagem = "";
	 numero = "";
	 largura = 0;
	 altura = 0;
	 numero_caracteres = 0;
	 tamanho_imagem = 0;
	 tamanho_cabecalho = 0;
	 cores = 0;
}

Imagem::~Imagem()
{

}

void Imagem::setStringArquivo(string string_arquivo) 
{
	this->string_arquivo = string_arquivo;
}
string Imagem::getStringArquivo() 
{
	return string_arquivo;
}

void Imagem::setCaminhoImagem(string caminho_imagem) 
{
	this->caminho_imagem = caminho_imagem;
}
string Imagem::getCaminhoImagem() 
{
	return caminho_imagem;
}

void Imagem::setNumero(string numero) 
{
	this->numero = numero;
}
string Imagem::getNumero() 
{
	return numero;
}

void Imagem::setLargura(int largura) 
{
	this->largura = largura;
}
int Imagem::getLargura() 
{
	return largura;
}

void Imagem::setAltura(int altura) 
{
	this->altura = altura;
}
int Imagem::getAltura() 
{
	return altura;
}

void Imagem::setNumeroCaracteres(int numero_caracteres) 
{
	this->numero_caracteres = numero_caracteres;
}
int Imagem::getNumeroCaracteres() 
{
	return numero_caracteres;
}

void Imagem::setTamanhoImagem(int tamanho_imagem) 
{
	this->tamanho_imagem = tamanho_imagem;
}
int Imagem::getTamanhoImagem() 
{
	return tamanho_imagem;
}

void Imagem::setTamanhoCabecalho(int tamanho_cabecalho) 
{
	this->tamanho_cabecalho = tamanho_cabecalho;
}
int Imagem::getTamanhoCabecalho() 
{
	return tamanho_cabecalho;
}

void Imagem::setCores(int cores) 
{
	this->cores = cores;
}
int Imagem::getCores() 
{
	return cores;
}

/*Abre a imagem (1), confere se a imagem foi aberta (2), calcula numero de caracteres (3) 
e os salva em uma string (4)*/
void Imagem::ManipulaArquivo() 
{
	//(1)
	ifstream arquivo;
	arquivo.open(caminho_imagem.c_str());
	//(2)
	if(arquivo.fail())
	{
		cout << "Erro ao abrir esse arquivo." << endl << endl;
		exit(0);
	}
	//(3) e (4)
	string string_arquivo;
	int numero_caracteres = 0;
	char c;
	while(!arquivo.eof()) 	//Enquanto nao estiver no final do arquivo
	{
		arquivo.get(c);
		string_arquivo += c;
		numero_caracteres++;
	}

	//cout << string_arquivo << endl << endl;
	//cout << numero_caracteres << endl << endl; 

	this->string_arquivo = string_arquivo;
	this->numero_caracteres = numero_caracteres;

	arquivo.close();
}

/*Extrai dados do cabeçalho como, numero magico (1), comentario (2),  largura (3), 
altura (4) e valor da escala de cores (5)*/
void Imagem::DadosCabecalho()
{
	string numero;
	string largura;
	string altura;
	string cores;
	int i = 0;

		while(string_arquivo[i] != '\n')	//Numero Magico
		{
			numero += string_arquivo[i];
			i++;
		}
		i++;

		if(string_arquivo[i] == '#')    //Comentario
		{
			while(string_arquivo[i] != '\n')
			{
				i++;
			}
			i++;
		}

			while(string_arquivo[i] != 	' ')	//Largura
			{
				largura += string_arquivo[i];
				i++;
			}
			i++;

			while(string_arquivo[i] != 	'\n')	//Altura
			{
				altura += string_arquivo[i];
				i++;
			}
			i++;
			

			while(string_arquivo[i] != '\n')	//Niveis de cor
			{
				cores += string_arquivo[i];
				i++;
			}
			i++;
			tamanho_cabecalho = i;
				
		
	this->numero = numero;
	this->largura = atoi(largura.c_str());
	this->altura = atoi(altura.c_str());
	this->cores = atoi(cores.c_str());
	this->tamanho_cabecalho = tamanho_cabecalho;

	//cout << numero << endl << largura << endl;
	//cout << altura << endl << cores << endl << tamanho_cabecalho << endl;

}


/*void Imagem::CalculaCabecalho()
{
	//cout << getNumeroCaracteres() << endl << getTamanhoImagem() << endl; 
	tamanho_cabecalho = getNumeroCaracteres() - getTamanhoImagem() - 1;

	this->tamanho_cabecalho = tamanho_cabecalho;

	cout << tamanho_cabecalho << endl;
}*/




