#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include "imagem.hpp"
#include "imagempgm.hpp"
#include "imagemppm.hpp"

using namespace std;

int main(int argc, char ** argv)
{
	string tipo, caminho_imagem;

	// Usuario define o tipo de imagem que sera tratada e o seu caminho em disco
	cout << "---------------------------------------------------------------------------" << endl;
	cout << "Este programa encontra mensagens escondidas em imagens do tipo .pgm e .ppm" << endl;
	cout << "---------------------------------------------------------------------------" << endl << endl;
	cout << "Digite o tipo da imagem (pgm ou ppm) ou sair para sair e aperte Enter: ";
	cin >> tipo;

	while(tipo!="ppm" && tipo!="pgm" && tipo!="sair") 
	{
		cout << "Tipo do arquivo digitado incorretamente ou nao e suportado pelo programa." << endl;
		cout << "Digite novamente utilizando somente letras minusculas e sem qualquer outro caracter: ";
		cin >> tipo;
		cout << endl;
	}

    if (tipo == "sair") 
    {
		cout << "Programa encerrado!" << endl;
		exit(0);
	}
	else 
	{
		cout << "Digite o local onde a imagem se encontra (Exemplo: /home/user/doc/segredo.ppm): ";
		cin >> caminho_imagem;
	}

	//Objetos a serem criados de acordo com o tipo definido pelo usuario
	if(tipo == "pgm")		//Imagem pgm
	{
		ImagemPgm *imagem;
		imagem = new ImagemPgm(caminho_imagem);

		imagem->ManipulaArquivo();
		imagem->DadosCabecalho();
		imagem->PegaInicio();
		imagem->VerificaImagem();
		imagem->TamanhoImagem();
		imagem->DecifraMensagem();

		delete(imagem);
	}

	else if(tipo == "ppm")		//Imagem ppm
	{
		ImagemPpm *imagem;
		imagem = new ImagemPpm(caminho_imagem);

		imagem->ManipulaArquivo();
		imagem->DadosCabecalho();
		imagem->VerificaImagem();
		imagem->TamanhoImagem();
		imagem->Filtros();

		delete(imagem);
	}


	return 0;
}