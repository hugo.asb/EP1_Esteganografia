#include "imagempgm.hpp"

ImagemPgm::ImagemPgm() 
{
	setStringArquivo("");
	setCaminhoImagem("");	
	setNumero("");		
	setLargura(0);
	setAltura(0);
	setNumeroCaracteres(0);
	setCores(0);
	setTamanhoImagem(0);
	setTamanhoCabecalho(0);
	posicao_inicial = 0;
	
}

ImagemPgm::~ImagemPgm()
{

}

ImagemPgm::ImagemPgm(string caminho_imagem)
{
	setStringArquivo("");
	setCaminhoImagem(caminho_imagem);	
	setNumero("");		
	setLargura(0);
	setAltura(0);
	setNumeroCaracteres(0);
	setCores(0);
	posicao_inicial = 0;
}

void ImagemPgm::setPosicaoInicial(int posicao_inicial) 
{
	this->posicao_inicial = posicao_inicial;
}
int ImagemPgm::getPosicaoInicial() 
{
	return posicao_inicial;
}

void ImagemPgm::PegaInicio()
{
	string posicao_inicial;
	int i = 0;
	
	while(getStringArquivo()[i] != ' ')
	{
		i++;
	}
	i++;
	while(getStringArquivo()[i] != ' ')
	{
		posicao_inicial += getStringArquivo()[i];
		i++;
	}

	this->posicao_inicial = atoi(posicao_inicial.c_str());

	//cout << posicao_inicial << endl;
}

void ImagemPgm::VerificaImagem() //Verifica se o tipo de imagem digitada e a mesma do arquivo
{
	if(getNumero() != "P5")
	{
		cout << "Essa imagem nao e do tipo .pgm!" << endl << endl;
		exit(0);
	}
}

void ImagemPgm::TamanhoImagem()
{
	int tamanho = getLargura() * getAltura();
	setTamanhoImagem(tamanho);
	//cout << tamanho << endl;
}

void ImagemPgm::DecifraMensagem() {

	int inicio = getTamanhoCabecalho() + getPosicaoInicial();   
	int controle = 0;           
	int repeticoes = 0;       
	int conta_hash = 0;       
	char b; 	//Bytes onde serao realizadas as operacoes de bitwise
	char b2;

	for(int i = inicio; i <= getNumeroCaracteres(); i++) 	// Percorre os bytes da imagem
	{
		controle++;
		
		if (controle == 1)
		{
			b = getStringArquivo()[i];   
			b = (b & 0x01);        //Operacao 'and' binaria para bit menos significativo
			b = (b << 1);          //Shift para mover o bit uma casa para a esquerda
		repeticoes++;
		}

		else if (controle == 2) 
		{
			b2 = getStringArquivo()[i];  
			b2 = (b2 & 0x01);      
			b = (b | b2);          //Operacao 'or' binaria
		repeticoes++;

			if (repeticoes < 8) 
			{
				b = (b << 1);    //Primeira repeticao: 00000xx0  
				controle = 1;   
			} 

			else {

				if (conta_hash == 0) 
				{
					conta_hash = 1;  

					cout << b;             
					controle = 0;       
					repeticoes = 0;   
				} 

				else 
				{
					if (b == '#') 
					{
					cout << endl << endl << "Mensagem esteganografada com sucesso" << endl; 
					break;
					} 

					else 
					{

				
						cout << b;              
						controle = 0;        
						repeticoes = 0;    
					}
				}
			}
		}
	}
}