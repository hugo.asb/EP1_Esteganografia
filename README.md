	---------------------------------------------------------------------------------------------
	- Este programa lê imagens do tipo .pgm e .ppm e encontra mensagens escondidas em seus bytes.
	---------------------------------------------------------------------------------------------


-> Para abrir o programa é necessário acessar o terminal do sistema operacional (como Linux ou Mac OS) e
abrir a pasta onde se encontra o arquivo Makefile e as pastas do programa (bin, doc, inc, obj, src).


-> Como o Makefile auxilia na compilação e execução da aplicação, no terminal, na pasta do programa, 
digite os sequintes códigos:
  -make (ou make all): Compila o programa;
  -make run: Roda o programa. Esse comando só funciona se o programa já foi compilado;
  -make clean: Apaga os objetos e o binario criados na compilação.


-> Depois de compilar e rodar o programa, siga os passos na tela. Preste atenção e digite corretamente o 
tipo do arquivo (pgm ou ppm) e o caminho (endereço) da imagem. 
- Exemplo de caminho: /home/user/Esteganografia/doc/lena.pgm


->A pasta .doc/ do programa contem alguns exemplos de imagens esteganografadas, porém o usuário pode utilizar
outras imagens salvas em disco.


-> Para as imagens do tipo pgm, a mensagem será impressa na tela do próprio terminal.


-> Para as imagens do tipo ppm serão criadas três novas imagens, uma para cada filtro - Red (vermelho), Green
(verde) e Blue (azul) - que são salvas no mesmo diretório onde está a imagem original, porém o nome da imagem
modificada é acrescida pela letra que representa o fitro no sistema RGB.
-Filtro Vermelho - R
-Filtro Verde    - G
-Filtro Azul     - B 


->Nas imagens do tipo ppm o usuário tem a liberdade de escolher a cor do filtro que ele deseja usar.



